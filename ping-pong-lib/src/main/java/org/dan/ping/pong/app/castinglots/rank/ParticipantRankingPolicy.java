package org.dan.ping.pong.app.castinglots.rank;

public enum ParticipantRankingPolicy {
    SignUp, ProvidedRating, History, Manual
}
