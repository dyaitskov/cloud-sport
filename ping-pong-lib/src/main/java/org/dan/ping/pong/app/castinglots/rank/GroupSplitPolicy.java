package org.dan.ping.pong.app.castinglots.rank;

public enum GroupSplitPolicy {
    BalancedMix,
    BestToBest
}
