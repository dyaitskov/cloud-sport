import './widget.module.js';
import './category-switch/category-switch.component.js';
import './complete-match-list/complete-match-list.component.js';
import './classic-group-view/participant-order.component.js';
import './classic-group-view/classic-group-view.component.js';
import './vis-ladder/vis-ladder.component.js';
import './group-switch/group-switch.component.js';
