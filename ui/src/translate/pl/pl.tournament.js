module.exports = {
    'Hidden': 'Ukryty',
    'Announce': 'Ogłoszony',
    'Draft': 'Rekrutacja',
    'Open': 'Otwarty',
    'Close': 'Zakończony',
    'Canceled': 'Anulowany',
    'Replaced': 'Przeniesiony',

    'tournament not found': 'Turniej {{tid}} nie istnieje'
};
