module.exports = {
        // place-detail
    'place name': 'Nazwa',
    'place city': 'Miasto',
    'Postal Address': 'Adres pocztowy',
    'Tables avaiable': 'Liczba stołów',
    'Contact Email': 'Email kontaktowy',
    'PlaceTitle': 'Miejsce',
    'PlaceWithName': 'Miejsce {{name}}',
    'AddTournament': 'Dodać turniej'
};
