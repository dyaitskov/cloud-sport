module.exports = {
    'tournament name': 'Nazwa',
    'tournament city': 'Miasto',
    'tournament place': 'Miejsce',
    'tournament date': 'Data',
    'participation price': 'Cena',
    'enlisted to tournament': 'Zapisane',
    'payment methods': 'Platność',
    'free': 'gratis',
    'cash': 'gotówka',
    'Categories': 'Kategorie',
    'My category': 'Moja kategoria',
    'you-have-been-expelled': 'Przykro mi powiedzieć, ale zostałeś wydalony z turneja.'
        + ' Poproś administratora o wyjaśnienie.',
    'open-tournament-resign-warn': 'Turniej jest już otwarty. Bierzesz w nim udział.'
        + ' Gdy teraz zrezygnujesz, nie możesz zmienić zdania!',
    'Resign btn': 'Zresygnować',
    'Enlist Me btn': 'Zapisać mnie',
    'previous tournament': 'Powszedni turniej w serii',
    'info-link-to-matches': '<a href="{{url}}">Tutaj</a> znajdziesz informację o meczach, w których musisz zagrać.',
    'thanks-for-enlistment-info': 'Dziękuje za zapis do turnieju <b>{{name}}</b>.'
        + ' Turneij zaczyna {{time | amTimeAgo}}.'
        + ' <a href="{{url}}">Tutaj</a> znajdziesz informację o meczach, w których musis zagrać.',
    'CategoryNotChosen': 'Kategoria nie zotała wybrana. Wybierz',
    'ChooseCategory': 'Wybierz kategorie',
    'Drafting to': 'Rekrutacja do {{name}}',
    'Resigning': 'Rezygnacja'
};
