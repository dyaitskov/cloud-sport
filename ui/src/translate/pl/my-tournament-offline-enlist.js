module.exports = {
        // my-tournament-offline-enlist
    'Ready to Play': 'Gotowy zagrać',
    'Just Enlist': 'Właśnie zapisany',
    'Right now enlisted': 'Teraz zapisane',
    'Offline enlist': 'Rekrutacja offline',
    'Offline enlist to': 'Offline rekrutacja do {{name}}',
    'Enlisting': 'Zapisywanie'
};
