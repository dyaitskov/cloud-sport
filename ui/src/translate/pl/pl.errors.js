module.exports = {
    'match-not-in-tournament': 'Mecz {{mid}} nie należy do turnieju',
    'unexpected-match-state': 'nieoczekiwany stan {{state}} meczu',
    'unknown-match-state': 'nieznany stan {{state}} meczu',
    'place-is-busy': 'Miejsce jest zajęte turniejem {{tid}}',
    'tournament-not-in-draft': 'Turniej {{tid}} nie pobiera i ma stan {{state}}'
};
