module.exports = {
    'Matches played by me': 'Matches played by me',

    'Opponent': 'Opponent',
    'PlayOff matches': 'PlayOff matches',
    'you-havent-played-matches': 'You haven\'t played any match in the tournament',
    'Group matches': 'Group matches',

    'Judged matches': 'Judged matches',

    'match-type-lbl': 'Type',
    'match-state-lbl': 'State',
    'to-score': 'Score',
    'Match management': 'Match management',
    'Schedule tables': 'Schedule tables',

    'Rescore match': 'Rescore match',

    'play-off-match-will-replayed': 'Dependent play off matches will be replayed, but you will have an opportunity to see consequences in advance.',
    'click-score-to-change': 'Click on a set score you want to change.',
    'Match score editor': 'Match score editor',

    'following-matches-will-be-replayed': 'Following matches will be cancelled:',

    'no-played-matches-in-tournament': 'Tournament has no played matches',

    'Total': 'Total',

    'Difference between games cannot be less than': 'Difference between games in the set {{set+1}} cannot be less than {{minAdvanceInGames}}',

    'Sets': 'Sets',
    'Set': 'Set',
    'Winner': 'Winner',
    'match-continues': 'Match continues...',
    'Group members': 'Group members',

    'won sets more that required': 'Match has to many won sets',

    'Results in groups': 'Results in groups',

    'dice-comment': 'The participant performed equally well with at least 2 others in the group, '
                        + 'so order between them is randomly chosen',

    'match-has-been-walked-over': 'Match has been complete due one of the participants resigned or get expelled',

    'group-play-off-border': 'Left in a group',

    'category-has-no-played-matches': 'Category has no played matches',

    'PlayOff ladder': 'PlayOff ladder'
};
