module.exports = {
        // place detail
    'place name': 'Name',
    'place city': 'City',
    'Postal Address': 'Postal Address',
    'Tables avaiable': 'Tables avaiable',
    'Contact Email': 'Contact Email',
    'PlaceTitle': 'Place',
    'PlaceWithName': 'Place {{name}}',
    'AddTournament': 'Add Tournament'
};
