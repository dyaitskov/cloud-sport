module.exports = {
    'match-not-in-tournament': 'Match {{mid}} is not associated with the tournament',
    'unexpected-match-state': 'Unexpected match state {{state}}',
    'unknown-match-state': 'Unknown match state {{state}}',
    'place-is-busy': 'Place is used by tournament {{tid}}',
    'tournament-not-in-draft': 'Tournament {{tid}} is not in draft state but {{state}}'
};
