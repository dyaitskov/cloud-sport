module.exports = {
    'participant-lbl': 'Participant',

    'Bid state': 'Participant state',

    'Want': 'Want',
    'Paid': 'Paid',
    'Here': 'Present',
    'Play': 'Plays',
    'Rest': 'Rest',
    'Wait': 'Wait for match',
    'Expl': 'Expelled',
    'Quit': 'Resigned',
    'Lost': 'Lost',
    'Win3': 'Bronze',
    'Win2': 'Silver',
    'Win1': 'Champion',

    'Enlisted to tournament': 'Enlisted to tournament'
};
