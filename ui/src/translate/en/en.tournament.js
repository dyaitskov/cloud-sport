module.exports = {
    'Hidden': 'Hidden',
    'Announce': 'Announce',
    'Draft': 'Draft',
    'Open': 'Open',
    'Close': 'Close',
    'Canceled': 'Canceled',
    'Replaced': 'Replaced',

    'tournament not found': 'Tournament {{tid}} does not exist'
};
