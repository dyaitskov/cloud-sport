module.exports = {
    'disambiguation-group-lbl': 'On same # of matches win',
    'CMP_WIN_AND_LOSE': 'Compare won and defeat scores',
    'CMP_WIN_MINUS_LOSE': 'Compare won minus defeat scores',
    'disambiguation-group-help': 'Decides how to order participants in a group'
        + ' having the same number of won matches. In case of won and defeat '
        + ' sum of gained scores across all matches are compared.'
        + ' If they are also the same'
        + ' then sums of lost scores across all matches are compared.'
        + ' In case of win minus defeat sum lost scores is subtracted from the sum of gained scores'
        + ' and then results are compared.',
    'thank-tournament-created-info': 'Thanks, tournament <a href="{{url}}">{{name}}</a>'
        + ' has been created and announced. Add categories to the tournament and begin drafting.',
    'Add categories btn': 'Add categories',
    'View tournament btn': 'View tournament',
    'Quits from a group': 'Quits from a group',
    'first-best': '1 best',
    'first-2-best': '2 best',
    'seeding-lbl': 'Seeding',
    'rank-direction-lbl': 'Order',
    'Sets to win a match': 'Number of sets',
    'group-schedule-help': 'Sets up an order for playing matches in a group.'
        + ' One line per case with a specified number of participants.'
        + ' An example for 3 participants in a group is'
        + ' <b>3: 1-2, 1-3, 2-3</b>.'
        + ' If there is no schedule for a group'
        + ' then matches will be scheduled according to the rules in PZTS 2016.',
    'Min score to win a set': 'Min set score',
    'min-score-to-win-set-help': 'Minimal set score to win a set',
    'Min score advance to win a set': 'Min advance',
    'min-score-advance-help': 'Required advance in the score to win a set',
    'decreasing-lbl': 'Descending',
    'increasing-lbl': 'Ascending',
    'rank-policy-lbl': 'Rating method',
    'provided-rating-policy': 'Provided skill',
    'manual-rating-policy': 'Manually',
    'sign-up-policy': 'Sign-up order',
    'max-losses': 'Max losses',
    'up-to-1-loss': 'up to 1',
    'up-to-2-losses': 'up to 2',
    'rank-name-lbl': 'Nazwa rankingu',
    'rank-name-length': 'Max length of rank name must be less than {{max}} letters',
    'Minimal allowed rank value': 'Min rating value',
    'Maximum allowed rank value': 'Max rating value',
    'quits-group-help': 'How many players quits from a group and continue participate'
        + ' in play-off tournament stage.',
    'Max group size': 'Max group size',
    'max-group-size-help': 'Maximum number of players, which could be in a group.',
    'Match score': 'Match score',
    'match-score-help': 'Determines how many sets an opponent should take, to win the match.',
    'Match for 3rd place': 'Match for 3rd place',
    'Back btn': 'Back',
    'Create & Open Draft': 'Create & Open Draft',
    'yes': 'yes',
    'no': 'no',
    'Publishing': 'Publishing',
    'group-size-less-quits': 'Max group size is less than number of people quitting group',
    'Tournament Parameters': 'Tournament Parameters',

    'unexpected-matches': 'There are unexpected matches [{{matches}}] encountered in the case for {{participants}} participants',
    'missing-matches': 'Following matches are missing [{{matches}}] in the case for {{participants}} participants',

    'Line has wrong match': 'Line [{{line}}] has wrong match [{{match}}]',
    'Line has no colon': 'Line [{{line}}] has no colon',
    'Number of participants not number': 'Line [{{line}}] has an error at the number of participants [{{number}}]',
    'tournament-rules-are-wrong': 'Tournament rules have errors',
    'no-group-nor-playoff': 'Tournament should have at least group or playoff'
};
