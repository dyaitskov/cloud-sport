module.exports = {
    'yes-lbl': 'Yes',
    'no-lbl': 'No',
    'group-lbl': 'Group',
    'match-lbl': 'Match',
    'play-off-lbl': 'Play Off',
    'saving': 'Saving'
};
