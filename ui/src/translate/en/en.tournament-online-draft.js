module.exports = {
        // tournament details page
    'tournament name': 'Name',
    'tournament city': 'City',
    'tournament place': 'Place',
    'tournament date': 'Date',
    'participation price': 'Price',
    'enlisted to tournament': 'Enlisted',
    'payment methods': 'Payment',
    'free': 'free',
    'cash': 'cash',
    'Categories': 'Categories',
    'My category': 'My category',
    'you-have-been-expelled': 'Sorry to say, but you have been expelled from this tournament.'
        + ' Ask administrator for clarification.',
    'open-tournament-resign-warn': 'Tournament is open. You are participate.'
        + ' If you resign right now, there is no way back!',
    'Resign btn': 'Resign',
    'Enlist Me btn': 'Enlist me',
    'previous tournament': 'Previous tournament in the seria',
    'info-link-to-matches': '<a href="{{url}}">Here</a> it is information about matches, you need to play in the tournament.',
    'thanks-for-enlistment-info': 'Thanks, you are enlisted to the tournament <b>{{name}}</b>.'
        + ' It will start {{time | amTimeAgo}}.'
        + ' <a href="{{url}}">Here</a> is information about'
        + ' matches, you need to play in the tournament.',
    'CategoryNotChosen': 'Category is not chosen. Choose',
    'ChooseCategory': 'Choose category',
    'Drafting to': 'Drafting to {{name}}',
    'Resigning': 'Resigning'
};
