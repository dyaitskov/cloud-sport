package org.dan.ping.pong.app.tournament;

import java.util.Optional;

public interface Enlist {
    int getCid();
    Optional<Integer> getProvidedRank();
}
