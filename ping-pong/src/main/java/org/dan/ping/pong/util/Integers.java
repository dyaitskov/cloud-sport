package org.dan.ping.pong.util;

public class Integers {
    public static boolean odd(int n) {
        return (n & 1) == 1;
    }
}
